import React, { Fragment } from "react";

import "./styles.css";
import NavBar from "./components/NavBar";
import Header from "./components/Header";
import Section from "./components/Section";
import Section2 from "./components/Section2";
import Section3 from "./components/Section3";
import Section4 from "./components/Section4";
import Section5 from "./components/Section5";
import Section6 from "./components/Section6";
import Section7 from "./components/Section7";
import Section8 from "./components/Section8";
import Section9 from "./components/Section9";
import Section10 from "./components/Section10";
import Section11 from "./components/Section11";
import Section12 from "./components/Section12";

const App = () => {
  return (
    <Fragment>
      <Header />
      <NavBar />
      <Section />
      <Section2 />
      <Section3 />
      <Section4 />
      <Section5 />
      <Section6 />
      <Section7 />
      <Section10 />
      <Section9 />
      <Section8 />
      <Section11 />
      <Section12 />
    </Fragment>
  );
};

export default App;

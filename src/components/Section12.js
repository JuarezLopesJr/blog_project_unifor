import React, { Fragment } from "react";

const Section12 = () => {
  return (
    <section className="sec4">
      <h1>Quinta Geração: Inteligência Artificial</h1>
      <p>
        Desde o início da Era dos computadores, os especialistas em informática
        trataram de desenvolver técnicas que permitem aos computadores actuar,
        como faz o ser humano. Uma das bases de apoio desta nova forma de
        desenhar um programa é a inteligência artificial. Tradicionalmente, a
        inteligência artificial é dividida em 3 grandes aplicações: - Os
        processos de linguagem natural, que facilita a comunicação do computador
        com o utilizador; - A robótica e tudo associado à visão e manipulação de
        objectos; - Os sistemas especialistas, baseados no armazenamento do
        conhecimento adquirido. A fim de visualizar a evolução dos micro
        processadores apresentaremos, como exemplo, o quadro de processadores da
        INTEL e a utilização de cada um deles.
      </p>
      <h2 style={{ color: "#ecf0f1" }}>Principais características:</h2>
      <p>
        O nascimento da Inteligência Artificial; Reconhecimento de voz; Sistemas
        inteligentes; Redes neuronais; Robótica; Redes de Alta Velocidade;
        Escala de Integração – ULSI: Ultra Large Scale Integration Computação
        Distribuída; Computação nas Nuvens (Cloud); Computação em Grade ou em
        Rede; Computação Móvel; Computação Ubíqua (presença directa das
        tecnologias na vida das pessoas, em casa ou em convívio social);
        Realidade Aumentada;
      </p>
      <h2 style={{ color: "#ecf0f1" }}>
        Processador Multi-núcleo (Multi-Core Processor)
      </h2>
      <p>
        São os processadores mais recentes e surgiram como alternativa para
        melhorar o desempenho e, ao mesmo tempo, reduzir o consumo de energia
        eléctrica. A ideia é melhorar o desempenho aproveitando a possibilidade
        de se executar processos de forma verdadeiramente paralela. Processador
        multi-núcleo é aquele formado por mais de um núcleo(core) de
        processamento. O multi-núcleo formado por dois núcleos é chamado
        Dual-Core. O multi-núcleo formado por quatro núcleos é chamado
        Quad-Core.
      </p>
      <h2 style={{ color: "#ecf0f1" }}>
        Exemplos de processadores multi-núcleo:
      </h2>
      <p>
        Pentium D, Pentium Extreme Edition, Athlon 64 X2, Athlon 64 FX (FX60 e
        superiores), Pentium Dual Core, Core 2 Duo, Core 2 Quad, Core 2 Extreme,
        Core i7, Dual-Core Intel Xeon processor series 5100, Quad-Core Intel
        Xeon processor series 5300
      </p>
    </section>
  );
};

export default Section12;

import React, { Fragment } from "react";

const Section4 = () => {
  return (
    <section className="sec4">
      <h1>História da Computação: Primeira Geração (1946 – 1959)</h1>
      <p>
        A primeira geração de computadores modernos tinha com principal
        característica o uso de válvulas eletrônicas, possuindo dimensões
        enormes. Eles utilizavam quilômetros de fios, chegando a atingir
        temperaturas muito elevadas, o que frequentemente causava problemas de
        funcionamento. Normalmente, todos os programas eram escritos diretamente
        na linguagem de máquina. Existiram várias máquinas dessa época, contudo,
        vamos focar no ENIAC, que foi a famosa de todas. No ano de 1946, ocorreu
        uma revolução no mundo da computação, como o lançamento do computador
        ENIAC (Electrical Numerical Integrator and Calculator), desenvolvido
        pelos cientistas norte-americanos John Eckert e John Mauchly. Esta
        máquina era em torno de 1000 vezes mais rápida que qualquer outra que
        existia na época. A principal inovação nesta máquina é a computação
        digital, muito superior aos projetos mecânicos-analógicos desenvolvidos
        até o exato momento. Com o ENIAC, a maioria das operações eram
        realizadas sem a necessidade de movimentar peças de forma manual, mas
        sim somente pela entrada de dados no painel de controle. Cada operação
        podia ser acessada através de configurações padrões de chaves e
        switches. As dimensões desta máquina são muito grandes, com
        aproximadamente 25 metros de comprimento por 5,50 m de altura. O seu
        peso total era de 30 toneladas. Esse valor representa algo como um andar
        inteiro de um prédio.
      </p>
    </section>
  );
};

export default Section4;

import React, { Fragment } from "react";

const Section8 = () => {
  return (
    <section className="sec4">
      <h1>Terceira Geração: Circuitos Integrados 1964 - 1980</h1>
      <p>
        A terceira geração começa em 1965 com a substituição dos transístores
        pela tecnologia dos circuitos integrados (CI). Os transístores e outros
        componentes electrónicos são miniaturizados e montados em um único chip.
        A finalização desta geração é datada no início dos anos 70 a qual foi
        considerada a importância de uma maior escala de integração para o
        início da 4ª geração.
      </p>
      <h2 style={{ color: "#ecf0f1" }}>Principais características:</h2>
      <h2 style={{ color: "#ecf0f1" }}>Altair 8800 (1975)</h2>
      <p>
        Primeiro computador pessoal portátil, produzido industrialmente para
        venda em massa.
      </p>
      <h2 style={{ color: "#ecf0f1" }}>Apple II (1976)</h2>
      <p>
        Lançado em 1976, por Steve Jobs e Steve Wozniak (fundadores da Apple
        Corp.) Foi o primeiro microcomputador pessoal a ter sucesso comercial
      </p>
    </section>
  );
};

export default Section8;

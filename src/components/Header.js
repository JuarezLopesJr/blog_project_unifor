import React from "react";

import logo from "../assets/debian.png";

const Header = () => {
  return (
    <header>
      <img src={logo} alt="LOGO" />
    </header>
  );
};

export default Header;

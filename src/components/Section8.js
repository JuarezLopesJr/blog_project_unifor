import React, { Fragment } from "react";

const Section8 = () => {
  return (
    <section className="sec4">
      <h1>Quarta Geração: Microprocessador 1980 - Actual</h1>
      <p>
        A quarta geração de computadores caracteriza-se pelo uso do
        microprocessador. O microprocessador é a CPU (Central Processing Unit)
        dos computadores,ou seja Unidade Central de Processamento. No início da
        década de 70, os CPUs possuíam a capacidade de processar por volta de
        100.000 informações por segundo e foram utilizados nos primeiros micros
        de 8 bits. CPU – Processador central de informações. É nesta pastilha de
        silício que são processadas todas as informações computacionais. INTEL –
        Um dos maiores fabricantes de processadores do mundo.
      </p>
      <h2 style={{ color: "#ecf0f1" }}>Principais características:</h2>
      <p>
        Introdução dos microprocessadores; Desenvolvimento dos computadores
        pessoais(PC); Evolução dos diversos componentes (hardware e software);
        Escala de Integração - VLSI: Very Large Scale Integration; Computadores
        pessoais e estações de trabalho; Sistemas operacionais MS-DOS, Windows e
        UNIX; Sistemas operacionais de rede; Evolução dos dispositivos diversos
        componentes (hardware e software); Micro- programação;
      </p>
      <h2 style={{ color: "#ecf0f1" }}>
        Evolução vertiginosa desde a sua introdução
      </h2>
      <h3 style={{ color: "#ecf0f1" }}>IBM PC (Personal Computer) (1981)</h3>
      <p>
        Lançado em 1981 pela IBM Corp. Primeiro microcomputador pessoal da IBM;
        O primeiro PC tinha clock de 4.77MHz, microprocessador Intel 8088 e
        usava o sistema operacional MS-DOS da Microsoft. Tornando-se o
        computador mais vendido de toda história.
      </p>
      <h3 style={{ color: "#ecf0f1" }}>Macintosh (1984)</h3>
      <p>
        Lançado em 1984; Primeiro computador a apresentar interface gráfico com
        o utilizador; Sucesso em vendas.
      </p>
      <h3 style={{ color: "#ecf0f1" }}>Intel 4004 (1971)</h3>
      <p>
        O Intel 4004 foi o primeiro processador lançado em um único chip de
        silício. Ele trabalhava com 4bits, sendo desenvolvido para o uso em
        calculadoras, operando com o clock máximo de 0.78 Mhz. Esta CPU
        calculava até 92.000 instruções por segundo(ou seja, cada instrução
        gastava 11 microssegundos).
      </p>
      <h3 style={{ color: "#ecf0f1" }}>Arquitetura x86 (década de 70)</h3>
      <p>
        A arquitectura x86, lançada em meados da década de 70, ainda serve como
        base para uma boa parte dos computadores actuais. O primeiro processador
        que aproveitou todo o seu potencial foi o Intel 8086, de 1978. Pela
        primeira vez, a velocidade do clock alcançou 5 MHz, utilizando
        instruções reais de 16bits, o dobro que suas versões concorrentes.{" "}
      </p>
      <h3 style={{ color: "#ecf0f1" }}>Os famosos 386 e 486 (década de 80)</h3>
      <p>
        As CPUs 80386 e 80486, trabalhavam com 40MHz e 100MHz, respectivamente.
        O 80386 permitiu que vários programas utilizassem o processador de forma
        cooperativa, através do escalonamento de tarefas. Já o 80486 foi o
        primeiro a usar o mecanismo de pipeline, permitindo que mais de uma
        instrução seja executada ao mesmo tempo no PC.
      </p>
    </section>
  );
};

export default Section8;

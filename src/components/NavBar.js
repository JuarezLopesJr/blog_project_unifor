import React from "react";

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li>
          <a href="#" className="active">
            Blog História da Computação
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;

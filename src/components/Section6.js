import React, { Fragment } from "react";

const Section4 = () => {
  return (
    <section className="sec4">
      <h1>Segunda Geração: Transístores 1957 - 1964</h1>
      <p>
        Nos equipamentos de segunda geração, a válvula foi substituída pelo
        transístor dispositivo electrónico desenvolvido em 1947 na BELL
        LABORATORIES por BARDEEN, BRETTAIN e SHOCKLE. Seu tamanho era 100 vezes
        menor que o da válvula, não precisava de tempo para aquecimento,
        consumia menos energia, era mais rápido e mais confiável. Os
        computadores desta época calculavam em microssegundos. Transístor –
        Dispositivo electrónico que serve para rectificar e ampliar os impulsos
        eléctricos. Com apenas 1/200 do tamanho de uma das primeiras válvulas e
        consumindo menos de 1/100 da energia ,o transístor passou a ser
        generalizado em todos os computadores por volta dos Anos 60. A função
        básica do transístor num computador é o de um interruptor electrónico
        para executar operações lógicas.
      </p>
      <p>
        <h2>TRADIC, da Bell Laboratories (1954 )</h2> O primeiro modelo de
        computador 100% transistorizado foi o TRADIC, da Bell Laboratories.
        (TRAnsistor DIgital Computer ou TRansistorized Airborne DIgital
        Computer). Outro modelo dessa época era o IBM 1401, com uma capacidade
        de memória base de 4.096 bytes. PDP-1{" "}
        <h2>(Programmed Data Processor-1) (1959)</h2> Foi o primeiro computador
        da serie da Corporação de Equipamento Digital foi o primeiro a ser
        produzido em 1959. É famoso devido ao facto de ter sido a criação mais
        importante na cultura de "Hacking" no MIT e de Bolt, Beranek e Newman. O
        PDP-1 também foi o Hardware original usado para jogar o primeiro
        video-jogo na história dos mini-computadores Steve Russell's Spacewar!.
      </p>
    </section>
  );
};

export default Section4;

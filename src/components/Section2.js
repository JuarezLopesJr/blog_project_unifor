import React, { Fragment } from "react";

const Section2 = () => {
  return (
    <section id="abaco" className="sec2">
      <h1>História da Computação</h1>
      <p>
        A capacidade dos seres humanos em calcular quantidades dos mais variados
        modos foi um dos fatores que possibilitaram o desenvolvimento da
        matemática e da lógica. Nos primórdios da matemática e da álgebra,
        utilizavam-se os dedos das mãos para efetuar cálculos. A mais antiga
        ferramenta conhecida para uso em computação foi o ábaco, e foi inventado
        na Babilônia por volta de 2400 a.C. O seu estilo original de uso, era
        desenhar linhas na areia com rochas. Ábacos, de um design mais moderno,
        ainda são usados como ferramentas de cálculo. O ábaco dos romanos
        consistia de bolinhas de mármore que deslizavam numa placa de bronze
        cheia de sulcos. Também surgiram alguns termos matemáticos: em latim
        "calx" significa mármore, assim "calculos" era uma bolinha do ábaco, e
        fazer cálculos aritméticos era "calculare". No século V a.C., na antiga
        Índia, o gramático Pānini formulou a gramática de Sânscrito usando 3959
        regras conhecidas como Ashtadhyāyi, de forma bastante sistemática e
        técnica. Pānini usou meta-regras, transformações e recursividade com
        tamanha sofisticação que sua gramática possuía o poder computacional
        teórico tal qual a máquina de Turing. Entre 200 a.C. e 400, os indianos
        também inventaram o logaritmo, e partir do século XIII tabelas
        logarítmicas eram produzidas por matemáticos islâmicos. Quando John
        Napier descobriu os logaritmos para uso computacional no século XVI,
        seguiu-se um período de considerável progresso na construção de
        ferramentas de cálculo. John Napier (1550-1617), escocês inventor dos
        logaritmos, também inventou os ossos de Napier, que eram tabelas de
        multiplicação gravadas em bastão, o que evitava a memorização da
        tabuada. A primeira máquina de verdade foi construída por Wilhelm
        Schickard (1592-1635), sendo capaz de somar, subtrair, multiplicar e
        dividir. Essa máquina foi perdida durante a guerra dos trinta anos,
        sendo que recentemente foi encontrada alguma documentação sobre ela.
        Durante muitos anos nada se soube sobre essa máquina, por isso,
        atribuía-se a Blaise Pascal (1623-1662) a construção da primeira máquina
        calculadora, que fazia apenas somas e subtrações. Pascal, que aos 18
        anos trabalhava com seu pai em um escritório de coleta de impostos na
        cidade de Rouen, desenvolveu a máquina para auxiliar o seu trabalho de
        contabilidade. A calculadora usava engrenagens que a faziam funcionar de
        maneira similar a um odômetro. Pascal recebeu uma patente do rei da
        França para que lançasse sua máquina no comércio. A comercialização de
        suas calculadoras não foi satisfatória devido a seu funcionamento pouco
        confiável, apesar de Pascal ter construído cerca de 50 versões. A
        máquina Pascal foi criada com objetivo de ajudar seu pai a computar os
        impostos em Rouen, França. O projeto de Pascal foi bastante aprimorado
        pelo matemático alemão Gottfried Wilhelm Leibniz (1646-1726), que também
        inventou o cálculo, o qual sonhou que, um dia no futuro, todo o
        raciocínio pudesse ser substituído pelo girar de uma simples alavanca.
        Em 1671, o filósofo e matemático alemão de Leipzig, Gottfried Wilhelm
        Leibniz introduziu o conceito de realizar multiplicações e divisões
        através de adições e subtrações sucessivas. Em 1694, a máquina foi
        construída, no entanto, sua operação apresentava muita dificuldade e era
        sujeita a erros. Em 1820, o francês natural de Paris, Charles Xavier
        Thomas, conhecido como Thomas de Colmar, projetou e construiu uma
        máquina capaz de efetuar as 4 operações aritméticas básicas: a
        Arithmomet. Esta foi a primeira calculadora realmente comercializada com
        sucesso. Ela fazia multiplicações com o mesmo princípio da calculadora
        de Leibniz e efetuava as divisões com a assistência do usuário. Todas
        essas máquinas, porém, estavam longe de serem consideradas um
        computador, pois não eram programáveis. Isto quer dizer que a entrada
        era feita apenas de números, mas não de instruções a respeito do que
        fazer com os números.
      </p>
    </section>
  );
};

export default Section2;
